-- DDL - Lenguaje de Definición de Dominio
drop database prueba;
create database prueba;
use prueba;
drop table tipo_documentos;
create table tipo_documentos (id_tipo_documento int, nombre varchar(255), siglas varchar(2) );
-- Adiciona una columna a la tabla tipo_documentos
alter table tipo_documentos add descripcion varchar(255);
-- Elimina la columna de la tabla tipo_documentos
alter table tipo_documentos drop descripcion;
-- Modifica la columna de la tabla tipo_documentos
alter table tipo_documentos modify nombre varchar(200);
-- Modificar el nombre d ela columna
alter table tipo_documentos rename column nombre to nombre1;

-- DML - Lenguaje de Manipulación de Datos
-- Create 
insert into tipo_documentos (id_tipo_documento, nombre, siglas) value (1, 'Cédula de ciudadania', 'CC');
insert into tipo_documentos (id_tipo_documento, nombre, siglas) value (2, 'Cédula de extranjeria', 'CE');
-- Read
select * from tipo_documentos;
-- Update
update tipo_documentos set nombre = 'Cédula de extranjería', siglas = 'CC' Where id_tipo_documento = 2;
-- Delete;
delete from tipo_documentos where id_tipo_documento = 1;

-- Restricciones de campos en mysql
NOT NULL - NULL  -- Campo no vacio o vacio
UNIQUE -- Campo único, no se puede repetir
PRIMARY KEY -- not null y unique, representa una fila de la tabla : Llave primaria
FOREIGN KEY -- Llave foránea - Permite relacionar ese campo con la llave primaria de otra tabla
AUTO_INCREMENT -- Permite incremntar automaticamente la llave primaria












-- Creación de la base de datos - ejercicio_practico
drop database ejercicio_practico;
create database ejercicio_practico;
use ejercicio_practico;
-- Creación de la tabla tipo_documentos
drop table tipo_documentos;
create table tipo_documentos (
	id_tipo_documento int8 PRIMARY KEY AUTO_INCREMENT, 
	nombre varchar(200) , 
	siglas varchar(3)
);
insert into tipo_documentos (nombre, siglas) value ('Cédula de ciudadania', 'CC');
insert into tipo_documentos (nombre, siglas) value ('Cédula de extranjeria', 'CE');
insert into tipo_documentos (nombre, siglas) value ('Número de identificación tributaria', 'NIT');
insert into tipo_documentos (nombre, siglas) value ('Tarjeta de identidad', 'TI');
insert into tipo_documentos (nombre, siglas) value ('Registro unico tributario', 'RUT');
delete from tipo_documentos where id_tipo_documento = 5;
select * from tipo_documentos;
--  tipo_documento
create table usuario (
	id_usuario int8 AUTO_INCREMENT, 
	nombre varchar(200), 
    apellido varchar(200),
    no_documento varchar(50) UNIQUE,
	usuario varchar(200), 
    pass varchar(200),
    id_tipo_documento int8,
    PRIMARY KEY (id_usuario),
    foreign key (id_tipo_documento) references tipo_documentos (id_tipo_documento)
);

select * from usuario;



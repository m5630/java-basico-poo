/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ficha2251585.crearclase.negocio;

/**
 *
 * @author YURA
 */
public class EjercicioClases {
    public static void main(String[] args) {
        String texto = "Hola, bienvenido";
        Persona persona = new Persona("Juan", "Perez", "CC", "8025874123687"); 
        System.out.println(persona.toString());
        persona.setNombre("Pedro");
        System.out.println(persona.toString());
        System.out.println(persona.despedirse());
     
        Persona personaVacia = new Persona();
        System.out.println(personaVacia);
        personaVacia.setNombre("Adriana");
        personaVacia.setApellido("Perez");
        personaVacia.setTipoDocumento("TI");
        System.out.println(personaVacia);
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.condicionales;

/**
 *
 * @author aiya
 */
public class Condicionales {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        /* Formato del condicional*/
        /* 1. Variable que no son boolean*/
        /* 2. Operador lógico -> <, >, ==, !=, */
        /* 3. Valor a comparar */
        String palo = "rojo"; 
        int valor = 1;
        if (palo.equals("rojo") && valor > 5 ) {
            //Instrucciones
            System.out.println("Pollito");
        }
        if (palo.equals("negro") || valor < 10 ) {
            //Instrucciones
            System.out.println("Vaca");
        }
        if (palo.equals("rojo") && valor == 1) {
            //Instrucciones
            System.out.println("Perro");
        }
        // Revisar && , ||
        // Y lógico -> && -> alt + 38
        // Arroz con pollo
        // var arroz
        // var pollo
        // Tengo arroz y no tengo pollo ->    False
        // No tengo arroz y tengo pollo ->    False
        // No tengo arroz y no tengo pollo -> False
        // Tengo arroz y tengo pollo ->       True
        boolean arroz = true; 
        boolean pollo = true;
        if(arroz && pollo){
            System.out.println("Hacer arroz con pollo");
            System.out.println("");
        }
        
        // O lógico -> || -> alt + 124
        // Cocinar
        // var arroz
        // var pollo
        // Tengo arroz o no tengo pollo ->    True
        // No tengo arroz o tengo pollo ->    True
        // No tengo arroz o no tengo pollo -> False
        // Tengo arroz o tengo pollo ->       True
        arroz = true; 
        pollo = true;
        if(arroz || pollo){
            System.out.println("Cocinar");
            System.out.println("");
        }
        if((arroz || (pollo && arroz)) || pollo){
        
        }
        
        /* Condicional if - else */
        if(arroz || pollo){
            // Instrucciones del if
            System.out.println("Cocinar");
        } 
        else {
            //Instrucciones el else
            System.out.println("Ir a la tienda");
        }
        
        /** Si anidados */
        if(arroz && pollo){
            // Instrucciones del if
            System.out.println("Arroz con pollo");
        } 
        else if(arroz || pollo){
            //Instrucciones el else
            System.out.println("Cocinar");
        }
        else
            System.out.println("Pedir domicilio");
        
        /* Switch **/
        int valores =5;
        switch(valores){
        case 1: System.out.println("Case 1 -> Valor es: "+valores);
                break;
        case 2: System.out.println("Case 2 -> Valor es: "+valores);
                break;
        case 3: System.out.println("Case 3 -> Valor es: "+valores);
                break;
        case 4: System.out.println("Case 4 -> Valor es: "+valores);
                break;
        default:    System.out.println("Default -> Otro valor: "+ valores);
                    break;
        }
        /* Switch **/
        String dia = "Lunes";
        switch(dia){
            case "lunes": System.out.println("Case 1 -> Valor es: "+dia);
                    break;
            case "martes": System.out.println("Case 2 -> Valor es: "+dia);
                    break;
            case "Miércoles": System.out.println("Case 3 -> Valor es: "+dia);
                    break;
            case "Jueves": System.out.println("Case 4 -> Valor es: "+dia);
                    break;
            default:    System.out.println("Default -> Otro valor: "+ dia);
                        break;
        }
        
        
        
    }
    
            
            
    /* Usted esta contratado para realizar el sistema de notas de un colegio **/
    /* Las notas las ingresa por consola y el programa le debe indicar la letra **/
    /* que el estudiante saca en la materia. El rango de notas es el siguiente: */
    /* La nota está entre 0 y 2.95 el sistema debe arrojar N -> No aprobado */
    /* La nota está entre 2.96 y 3.95 el sistema debe arrojar A -> Aprobado */
    /* La nota está entre 3.96 y 4.95 el sistema debe arrojar B -> Bueno */
    /* La nota está entre 4.96 y 5 el sistema debe arrojar E -> Excelente */
    /* Si la nota no pertenece a ningún rango anteriormente estipulado debe arrojar "error en la nota" */
            
}

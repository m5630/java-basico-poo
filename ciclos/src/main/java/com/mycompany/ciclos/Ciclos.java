/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.ciclos;

/**
 *
 * @author aiya
 */
public class Ciclos {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        
        /* for , while, do-while */
        
        /** FOR */
        System.out.println("Ciclo FOR");
        /* Formato del condicional*/
        /* 1. Variable que no son boolean*/
        /* 2. Operador lógico -> <, >, ==, !=, */
        /* 3. Valor a comparar */
        for (int i = 5; i > 0; i--) {
            System.out.println("i: "+i);
        }
        
        /** WHILE */
        System.out.println("Ciclo WHILE");
        /* Formato del condicional*/
        /* 1. Variable que no son boolean*/
        /* 2. Operador lógico -> <, >, ==, !=, */
        /* 3. Valor a comparar */
        int i = 5;
        while (i > 0) {            
            System.out.println("i: "+i);
            i = i - 1;
        }
        
        /** DO - WHILE */
        System.out.println("Ciclo DO - WHILE");
        /* Formato del condicional*/
        /* 1. Variable que no son boolean*/
        /* 2. Operador lógico -> <, >, ==, !=, */
        /* 3. Valor a comparar */
        i = 5;
        do{
            System.out.println("i: "+i);
            i--;
        }while(i>0);
    }
    
    /** Realizar un algoritmo que permita ingresar un número y el 
     software devuelva la serie fibonacci del número */
    
    
    
    
}

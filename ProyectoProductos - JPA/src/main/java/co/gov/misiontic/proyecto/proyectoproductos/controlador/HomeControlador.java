/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.gov.misiontic.proyecto.proyectoproductos.controlador;

import co.gov.misiontic.proyecto.proyectoproductos.vista.Home;

/**
 *
 * @author YURA
 */
public class HomeControlador {
    
    public static Home home = new Home();
    
    public static void mostrar(){
        home.setVisible(true);
    }
    public static void ocultar(){
        home.setVisible(false);
    }

    public static void salir() {
        ocultar();
        LoginControlador.mostrar();
    }
}

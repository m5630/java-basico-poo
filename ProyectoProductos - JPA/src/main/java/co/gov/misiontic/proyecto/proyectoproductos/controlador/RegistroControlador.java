/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.gov.misiontic.proyecto.proyectoproductos.controlador;

import co.gov.misiontic.proyecto.proyectoproductos.entidades.TipoDocumento;
import co.gov.misiontic.proyecto.proyectoproductos.entidades.Usuario;
import co.gov.misiontic.proyecto.proyectoproductos.repositorio.ITipoDocumentoRepositorio;
import co.gov.misiontic.proyecto.proyectoproductos.repositorio.IUsuarioRepositorio;
import co.gov.misiontic.proyecto.proyectoproductos.repositorio.TipoDocumentoRepositorio;
import co.gov.misiontic.proyecto.proyectoproductos.repositorio.UsuarioRepositorio;
import co.gov.misiontic.proyecto.proyectoproductos.vista.Registro;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author YURA
 */
public class RegistroControlador {
    
    public static ITipoDocumentoRepositorio tipoDocumentoRepositorio = new TipoDocumentoRepositorio();
    public static IUsuarioRepositorio usuarioRepositorio = new UsuarioRepositorio();
    public static Registro registro  = new Registro();
    
    public static void mostrar(){        
        registro.setVisible(true);
        cargarComboBoxTipoDoc();
    }
    public static void ocultar(){
        registro.setVisible(false);
    }
    public static void cargarComboBoxTipoDoc(){
        List<TipoDocumento> tiposDeDocumentos = tipoDocumentoRepositorio.getTipoDocumentos();
        registro.comboBoxTipoDocumento.addItem("Seleccione");
        for (TipoDocumento tipoDeDocumento : tiposDeDocumentos) {
            registro.comboBoxTipoDocumento.addItem(tipoDeDocumento.getNombre());  
        }   
    }
    
   public static void crearUsuario(){
       Usuario user = new Usuario();
       user.setApellido(registro.textApellido.getText());
       //user.setIdUsuario(1);
       user.setNoDocumento(registro.textNoDocumento.getText());
       user.setNombre(registro.textNombre.getText());
       user.setPassword(registro.textPassword.getText());
       user.setUsuario(registro.textUsuario.getText());
       TipoDocumento tipoDocumento = new TipoDocumento();
       List<TipoDocumento> tiposDeDocumentos = tipoDocumentoRepositorio.getTipoDocumentos();
       String valorCombo = (String) registro.comboBoxTipoDocumento.getSelectedItem();
       for (TipoDocumento tiposDeDocumento : tiposDeDocumentos) {
           if(tiposDeDocumento.getNombre().equals(valorCombo))
               tipoDocumento = tiposDeDocumento;
       }
       System.out.println(tipoDocumento);
       user.setTipoDocumento(tipoDocumento);
       System.out.println(user.toString());
       Usuario usuario = usuarioRepositorio.agregarUsuario(user);
       if(usuario.getIdUsuario() == null)
           JOptionPane.showInputDialog(null, "Error al guardar el registro", "El usuario no puede ser agregado, consulte al adminitrador", JOptionPane.ERROR_MESSAGE);
       else{
           ocultar();
           HomeControlador.mostrar();
           registro.textApellido.setText("");
           registro.textNoDocumento.setText("");
           registro.textNombre.setText("");
           registro.textPassword.setText("");
           registro.textUsuario.setText("");
           registro.comboBoxTipoDocumento.setSelectedIndex(-1);
       }
   }
}

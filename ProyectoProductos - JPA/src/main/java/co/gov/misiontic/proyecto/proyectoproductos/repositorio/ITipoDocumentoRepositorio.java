/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.gov.misiontic.proyecto.proyectoproductos.repositorio;

import co.gov.misiontic.proyecto.proyectoproductos.entidades.TipoDocumento;
import java.util.List;

/**
 *
 * @author YURA
 */
public interface ITipoDocumentoRepositorio {
    
    public TipoDocumento agregarTipoDocumento(TipoDocumento tipoDocumento);
    
    public List<TipoDocumento> getTipoDocumentos();
    
    public TipoDocumento editarTipoDocumento(TipoDocumento tipoDocumento);
    
    public void eliminarTipoDocumento(int idTipoDocumento);
}

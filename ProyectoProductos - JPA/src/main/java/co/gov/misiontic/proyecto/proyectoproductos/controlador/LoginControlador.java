/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.gov.misiontic.proyecto.proyectoproductos.controlador;

import co.gov.misiontic.proyecto.proyectoproductos.entidades.Usuario;
import co.gov.misiontic.proyecto.proyectoproductos.repositorio.IUsuarioRepositorio;
import co.gov.misiontic.proyecto.proyectoproductos.repositorio.UsuarioRepositorio;
import co.gov.misiontic.proyecto.proyectoproductos.vista.Login;
import javax.swing.JOptionPane;

/**
 *
 * @author YURA
 */
public class LoginControlador {
    
    
    public static IUsuarioRepositorio usuarioRepositorio = new UsuarioRepositorio();
    public static Login login = new Login();
    public static void mostrar(){        
        login.setVisible(true);
    }
    public static void ocultar(){
        login.setVisible(false);
    }
    public static void ingresoButton(){   
        Usuario user = new Usuario();
        user.setUsuario(login.textUser.getText());
        user.setPassword(login.textPass.getText());
        user = usuarioRepositorio.getLoginUsuario(user);
        if(user.getUsuario() == null || user.getUsuario().isEmpty())
            JOptionPane.showInternalMessageDialog(null, "Revise su usuario y/o password", "Error al ingresar" , JOptionPane.ERROR_MESSAGE );
        else{
            ocultar();
            HomeControlador.mostrar();
            login.textUser.setText("");
            login.textPass.setText("");
        }      
    }
    
    public static void nuevoRegistro(){
        ocultar();
        RegistroControlador.mostrar();
    }

}

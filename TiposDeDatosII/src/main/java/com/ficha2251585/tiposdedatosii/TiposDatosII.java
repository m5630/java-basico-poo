/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ficha2251585.tiposdedatosii;

/**
 *
 * @author YURA
 */
public class TiposDatosII {
    
    public static void main(String[] args) {
        /* Tipos de datos primitivos */
        /* Decimales -> float, double */
        /*Float*/
        /* 2 ^ 32 =  */
        float numeroDecimalFloat = 3.5F;
        System.out.println("Valor: "+ numeroDecimalFloat);
        System.out.println("Valor minímo de float: "+Float.MIN_VALUE);
        System.out.println("Valor máximo de float: " + Float.MAX_VALUE);
        
        /* Double */
        /* 2 ^ 64 */
        double numeroDecimalDouble = 3.5;
        System.out.println("Valor "+ numeroDecimalDouble);
        System.out.println("Valor mínimo de docuble: " + Double.MIN_VALUE);
        System.out.println("Valor máximo de double: "+ Double.MAX_VALUE);
        
        /* Boolean */
        /* */
        boolean valorBooleano = false;
        System.out.println("Valor: "+valorBooleano);
        
        /** Caracter */
        /* */
        char valorCaracter = 'P';
        System.out.println("Valor: "+valorCaracter);
        
        /* Var -> Cualquier tipo de dato*/
        var valorVar = "Texto";
        
        /* Tipo de datos objeto */
        /* String */
        String valorTexto = "UN TEXTO";
        System.out.println("valorTexto: "+valorTexto);
        
        /* Clases creados por el desarrolador */
        // Usuario user;
    }
    
    
    
}

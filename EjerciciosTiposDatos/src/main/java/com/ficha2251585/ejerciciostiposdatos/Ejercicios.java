/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ficha2251585.ejerciciostiposdatos;

import java.util.Scanner;

/**
 *
 * @author YURA
 */
public class Ejercicios {
    
    public static void main(String[] args) {
       //Convertir enteros
        var edad = "50"; 
        int edadInt = Integer.parseInt(edad); //Excepción
        System.out.println("Edad: "+edad);
        System.out.println("Edad entero "+edadInt);
        
        // Convertir Decimales
        var pi = "3.1416";
        double piDouble = Double.parseDouble(pi);
        System.out.println("pi: "+pi);
        System.out.println("pi double: "+piDouble);
        
        var consola = new Scanner(System.in);
        System.out.println("Digite la edad: ");
        int captura = Integer.parseInt(consola.nextLine());
        System.out.println(captura);
        
        //String
        String edadTexto = String.valueOf(captura);
        System.out.println("edad en texto: "+edadTexto);
        
        //Caracter
        char caracter = "Hola".charAt(0);
        System.out.println("Primer caracter de Hola: "+caracter);
        caracter = edadTexto.charAt(0);
        System.out.println("Primer caracter de la edad: "+caracter);
    }
}

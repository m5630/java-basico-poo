/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.gov.misiontic.proyecto.proyectoproductos.entidades;

/**
 *
 * @author YURA
 */
public class TipoDocumento {
    
    private Integer idTipodocumento;
    private String nombre;
    private String siglas;

    public Integer getIdTipodocumento() {
        return idTipodocumento;
    }

    public void setIdTipodocumento(Integer idTipodocumento) {
        this.idTipodocumento = idTipodocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSiglas() {
        return siglas;
    }

    public void setSiglas(String siglas) {
        this.siglas = siglas;
    }

    @Override
    public String toString() {
        return "TipoDocumento{" + "idTipodocumento=" + idTipodocumento + ", nombre=" + nombre + ", siglas=" + siglas + '}';
    }
    
    public int sumar (int a, int b){
        return a + b;
    }
    
}

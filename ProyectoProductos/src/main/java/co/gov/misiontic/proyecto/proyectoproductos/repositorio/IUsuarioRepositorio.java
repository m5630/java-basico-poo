/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.gov.misiontic.proyecto.proyectoproductos.repositorio;

import co.gov.misiontic.proyecto.proyectoproductos.entidades.Usuario;
import java.util.List;

/**
 *
 * @author YURA
 */
public interface IUsuarioRepositorio {
    
    public Usuario agregarUsuario(Usuario user);
    
    public List<Usuario> getUsuarios();
    
    public Usuario editarUsuario(Usuario user);
    
    public void eliminarUsuario(int idUsuario);
    
    public Usuario getLoginUsuario(Usuario user);
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.gov.misiontic.proyecto.proyectoproductos.repositorio;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author YURA
 */
public class Conexion {
    
    //Conexión
    Connection con;
    public Connection getConnection(){
        String url = "jdbc:mysql://192.168.1.5:3306/ejercicio_practico";
        String user = "root";
        String password = "1234";
        try {
            //Drivers
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection(url, user, password);
            System.out.println("Conexión éxitosa");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return con;
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.gov.misiontic.proyecto.proyectoproductos.repositorio;

import co.gov.misiontic.proyecto.proyectoproductos.entidades.TipoDocumento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author YURA
 */
public class TipoDocumentoRepositorio implements ITipoDocumentoRepositorio{

    Conexion conexion = new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet res;
    
    @Override
    public TipoDocumento agregarTipoDocumento(TipoDocumento tipoDocumento) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<TipoDocumento> getTipoDocumentos() {
        System.out.println("getTipoDocumentos");
        List<TipoDocumento> tiposDocumentos = new ArrayList<>();
        con = conexion.getConnection();
        try {
            ps = con.prepareStatement("select * from tipo_documentos");
            res = ps.executeQuery();
            while(res.next()){
                TipoDocumento td = new TipoDocumento();
                td.setIdTipodocumento(res.getInt(1));
                td.setNombre(res.getString(2));
                td.setSiglas(res.getString(3));
                System.out.println(td.toString());
                tiposDocumentos.add(td);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoDocumentoRepositorio.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*
        TipoDocumento tipoDocumento1 = new TipoDocumento();
        tipoDocumento1.setIdTipodocumento(1);
        tipoDocumento1.setNombre("Cédula de ciudadanía");
        tipoDocumento1.setSiglas("CC");
        tiposDocumentos.add(tipoDocumento1);
        TipoDocumento tipoDocumento2 = new TipoDocumento();
        tipoDocumento2.setIdTipodocumento(2);
        tipoDocumento2.setNombre("Cedula de extranjería");
        tipoDocumento2.setSiglas("CE");
        tiposDocumentos.add(tipoDocumento2);
        */
        return tiposDocumentos;
    }

    @Override
    public TipoDocumento editarTipoDocumento(TipoDocumento tipoDocumento) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void eliminarTipoDocumento(int idTipoDocumento) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.gov.misiontic.proyecto.proyectoproductos.repositorio;

import co.gov.misiontic.proyecto.proyectoproductos.entidades.TipoDocumento;
import co.gov.misiontic.proyecto.proyectoproductos.entidades.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author YURA
 */
public class UsuarioRepositorio implements IUsuarioRepositorio{
    Conexion conexion = new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet res;

    //CRUD
    //Create
    @Override
    public Usuario agregarUsuario(Usuario user){
        System.out.println("agregarUsuario");
        Usuario usuario = new Usuario();
        con = conexion.getConnection();
        try {
            // Insertar el usuario
            ps = con.prepareStatement("insert into usuario(nombre, apellido, no_documento,  usuario, pass, id_tipo_documento) values(?,?,?,?,?,?)");
            ps.setString(1, user.getNombre());
            ps.setString(2, user.getApellido());
            ps.setString(3, user.getNoDocumento());
            ps.setString(4, user.getUsuario());
            ps.setString(5, user.getPassword());
            ps.setInt(6, user.getTipoDocumento().getIdTipodocumento());
            int i = ps.executeUpdate();
            // Buscar el usuaro agregado
            ps = con.prepareStatement("select * from usuario as u where u.usuario = ?");
            ps.setString(1, user.getUsuario());
            res = ps.executeQuery();
            while(res.next()){
                usuario.setIdUsuario(res.getInt(1));
                usuario.setNombre(res.getString(2));
                usuario.setApellido(res.getString(3));
                usuario.setNoDocumento(res.getString(4));
                usuario.setUsuario(res.getString(5));
                usuario.setPassword(res.getString(6));
                TipoDocumento td = new TipoDocumento();
                td.setIdTipodocumento(res.getInt(7));
                usuario.setTipoDocumento(td);
            }
            con.close();
            System.out.println(usuario.toString());
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioRepositorio.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return usuario;
    }
    //Read
    @Override
    public List<Usuario> getUsuarios(){
        List<Usuario> usuarios = new ArrayList<>();
        return usuarios;
    }
    //Update
    @Override
    public Usuario editarUsuario(Usuario user){
        Usuario usuario = new Usuario();
        return usuario;
    }
    //Delete
    @Override
    public void eliminarUsuario(int idUsuario){
        
    }
    @Override
    public Usuario getLoginUsuario(Usuario user){
        System.out.println("getLoginUsuario");
        Usuario usuario = new Usuario();
        usuario.setUsuario("marcelita");
        usuario.setPassword("1234");
        if(user.getUsuario().equals(usuario.getUsuario()) && 
                user.getPassword().equals(usuario.getPassword()))
            return usuario;
        else                  
            return new Usuario();
    }
}

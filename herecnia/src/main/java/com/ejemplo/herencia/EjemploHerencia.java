/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ejemplo.herencia;

/**
 *
 * @author YURA
 */
public class EjemploHerencia {

    public static void main(String[] args) {
        System.out.println("Herencia");
        // Objetos vacios
        // Objeto padre
        System.out.println("Objeto Padre");
        Figura figura = new Figura();
        figura.metodoPadre();
        // Objeto Hijo
        System.out.println("Objeto Hijo");
        FiguraDosD figuraHijo = new FiguraDosD(); 
        figuraHijo.metodoHijo();
        figuraHijo.metodoPadre();
        
        // Herencia Atributos
        // Objeto padre
        System.out.println("Objeto Padre - Atributos");
        Figura figura1 = new Figura("Figura","Rojo");
        figura1.metodoPadre();
        System.out.println(figura1.toString());
        // Objeto Hijo
        System.out.println("Objeto Hijo - Atributos solo padre");
        FiguraDosD figuraHijo1 = new FiguraDosD("Figura","Rojo"); 
        figuraHijo1.metodoHijo();
        figuraHijo1.metodoPadre();
        figuraHijo1.setLado("Tres");
        System.out.println(figuraHijo1.toString());
        
        System.out.println("Objeto Hijo - Atributos completos");
        FiguraDosD figuraHijo2 = new FiguraDosD("Figura","Rojo","cinco"); 
        figuraHijo2.metodoHijo();
        figuraHijo2.metodoPadre();
        System.out.println(figuraHijo2.toString());
        
        System.out.println("Objeto Hijo - Sin Atributos");
        FiguraDosD figuraHijo3 = new FiguraDosD(); 
        figuraHijo3.metodoHijo();
        figuraHijo3.metodoPadre();
        figuraHijo3.setColor("Azul");
        //figuraHijo3.setLado("Diez");
        figuraHijo3.setNombre("Figura");
        System.out.println(figuraHijo3.toString());
        
        // Clase cuadrado 
        System.out.println("Clase cuadrado");
        Cuadrado cuadrado = new Cuadrado("Cuadrado","rojo","cinco","diez", "veinticinco");
        System.out.println(cuadrado.toString());
        cuadrado.metodoPadre();
        
        // Clase circulo
        System.out.println("Clase circulo");
        Circulo circulo = new Circulo("cinco","diez");
        circulo.metodoHijo();
        circulo.metodoPadre();
    }
}

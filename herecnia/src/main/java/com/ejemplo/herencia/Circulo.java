/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ejemplo.herencia;

/**
 *
 * @author YURA
 */
public class Circulo extends FiguraDosD{
    
    private String perimetro;
    private String area;

    public Circulo(String perimetro, String area) {
        this.area = area;
        this.perimetro = perimetro;
    }

    public String getPerimetro() {
        return perimetro;
    }

    public void setPerimetro(String perimetro) {
        this.perimetro = perimetro;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
    
    
}

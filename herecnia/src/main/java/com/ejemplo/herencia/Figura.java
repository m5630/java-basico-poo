/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ejemplo.herencia;

/**
 *
 * @author YURA
 */
public class Figura {
    
    private String nombre;
    private String color;

    public Figura(String nombre, String color) {
        this.nombre = nombre;
        this.color = color;
    }

    public Figura() {
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    public void metodoPadre(){
        System.out.println("Metodo de la clase Figura - Padre");
    }

    // Polimorfismo
    @Override
    public String toString() {
        return "Figura{" + "nombre=" + nombre + ", color=" + color + '}';
    }
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ejemplo.herencia;

/**
 *
 * @author YURA
 */
public class FiguraDosD extends Figura{
    
    private String lado;
    
    public FiguraDosD(String nombre, String color, String lado) {
        super(nombre, color);
        this.lado = lado;
    }
    
    public FiguraDosD(String nombre, String color) {
        super(nombre, color);
    }

    public FiguraDosD() {
    }
    
    public String getLado() {
        return lado;
    }

    public void setLado(String lado) {
        this.lado = lado;
    }
    
    public void metodoHijo(){
        System.out.println("Hola desde la clase figura 2 D - Hijo");
    }

    @Override
    public String toString() {
        return "FiguraDosD{" + "lado=" + lado + '}';
    }
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ejemplo.herencia;

/**
 *
 * @author YURA
 */
public class Cuadrado extends FiguraDosD{
    
    private String perimetro;
    private String area;

    public Cuadrado(String nombre, String color, String lado, String perimetro, String area) {
        super(nombre, color, lado);
        this.perimetro = perimetro;
        this.area = area;
    }

    public Cuadrado() {
    }

    public String getPerimetro() {
        return perimetro;
    }

    public void setPerimetro(String perimetro) {
        this.perimetro = perimetro;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return "Cuadrado{" + "perimetro=" + perimetro + ", area=" + area + '}';
    }
    
    @Override
    public void metodoPadre(){
        System.out.println("Metodo de la clase Cuadrado, ya no viene con los valores de padre");
    }

    
    
    


}

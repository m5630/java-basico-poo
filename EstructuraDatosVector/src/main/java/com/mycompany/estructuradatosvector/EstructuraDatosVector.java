/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.estructuradatosvector;

/**
 *
 * @author aiya
 */
public class EstructuraDatosVector {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        /* Estructura de datos estáticas*/
        /* Vector - unidimensional*/
        System.out.println("Vector double");
        double [] vectorDouble = new double [6];
        for (int i = 0; i < vectorDouble.length; i++) {
            System.out.println("Posición "+i + " - Valor "+vectorDouble[i]);
        }
        for (int i = 0; i < vectorDouble.length; i++) {
            vectorDouble[i]=2.5+2*i; 
            System.out.println("Posición "+i + " - Valor "+vectorDouble[i]);
        }
        System.out.println("Vector int");
        int [] vectorInt = {1, 28, 10};
        for (int i = 0; i < vectorInt.length; i++) {
            System.out.println("Posición "+i + " - Valor "+vectorInt[i]);
        }
        final int FILAS = 2, COLUMNAS = 3;
        /* Matrices - Bidimensional */
        int [][] matrizDouble = new int [FILAS][COLUMNAS];
        System.out.println("Matriz double");
        for (int i = 0; i < FILAS; i++) {
            for (int j = 0; j < COLUMNAS; j++) {
                matrizDouble[i][j]= 2+i*j;
                System.out.println("matriz["+i+"]["+j+"] - valor: "+matrizDouble[i][j]);
            }
        }  
        System.out.println("Matriz int");
        int [][] matrizInt = {{1,2,3},{4,5,6}};
        for (int i = 0; i < matrizInt.length; i++) {
            for (int j = 0; j < matrizInt[i].length; j++) {
                System.out.println("matriz["+i+"]["+j+"] - valor: "+matrizInt[i][j]);
            }
        }  
    }
}
/**
    En un vector, almacenar los valores de la serie fibonacci que digita el usuario
    * 0 -> F0 = 0
    * 1 -> F1 = 0 1
    * 
    * 8 -> F8 = 0 1 1 2 3 5 8 13 21
* */
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ficha2251585.ejercicioclases;

/**
 *
 * @author YURA
 */
public class Empleado {
    private String primerNombre;
    private String primerApellido;
    private Integer mes;
    private Double salario;

    public Empleado(String primerNombre, String primerApellido, Integer mes, Double salario) {
        this.primerNombre = primerNombre;
        this.primerApellido = primerApellido;
        this.mes = mes;
        this.salario = salario;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        if(salario < 0.0)
            this.salario = 0.0;
        else
            this.salario = salario;
    }

    @Override
    public String toString() {
        return "Empleado{" + "primerNombre=" + primerNombre + ", primerApellido=" + primerApellido + ", mes=" + mes + ", salario=" + salario + '}';
    }
    
    
}

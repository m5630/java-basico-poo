/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ficha2251585.ejercicioclases;

/**
 *
 * @author YURA
 */
public class PruebaEmpleado {

    public static void main(String[] args) {
        // Para 1 empleado
        Double [] salario = new Double [13]; 
        Empleado empleadoEnero = new Empleado("Alexander", "Diaz",1,250000.0);
        Empleado empleadoFebrero = new Empleado("Alexander", "Diaz",2,250000.0);
        Empleado empleadoMarzo = new Empleado("Alexander", "Diaz",3,250000.0);
        Empleado empleadoAbril = new Empleado("Alexander", "Diaz",4,250000.0);
        Empleado empleadoMayo = new Empleado("Alexander", "Diaz",5,250000.0);
        Empleado empleadoJunio = new Empleado("Alexander", "Diaz",6,250000.0);
        Empleado empleadoJulio = new Empleado("Alexander", "Diaz",7,250000.0);
        Empleado empleadoAgosto = new Empleado("Alexander", "Diaz",8,250000.0);
        Empleado empleadoSeptiembre = new Empleado("Alexander", "Diaz",9,250000.0);
        Empleado empleadoOctubre = new Empleado("Alexander", "Diaz",10,250000.0);
        Empleado empleadoNoviembre = new Empleado("Alexander", "Diaz",11,250000.0);
        Empleado empleadoDiciembre = new Empleado("Alexander", "Diaz",12,0.0);
        empleadoDiciembre.setSalario(-25000.0);
        salario[0] = empleadoEnero.getSalario();
        salario[1] = empleadoFebrero.getSalario();
        salario[2] = empleadoMarzo.getSalario();
        salario[3] = empleadoAbril.getSalario();
        salario[4] = empleadoMayo.getSalario();
        salario[5] = empleadoJunio.getSalario();
        salario[6] = empleadoJulio.getSalario();
        salario[7] = empleadoAgosto.getSalario();
        salario[8] = empleadoSeptiembre.getSalario();
        salario[9] = empleadoOctubre.getSalario();
        salario[10] = empleadoNoviembre.getSalario();
        salario[11] = empleadoDiciembre.getSalario();
        salario[12]=0.0;
        for (int i = 0; i < salario.length-1; i++) {
            salario[salario.length-1] = salario[salario.length - 1] + salario[i];
        }
        for (int i = 0; i < salario.length; i++) {
            System.out.println(salario[i]);
        }
        
    }
    
}

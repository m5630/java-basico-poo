/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ejemplo.listas;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 *
 * @author YURA
 */
public class Home {
    public static void main(String[] args) {
        System.out.println("Listas");
        //Definición de una lista
        List<String> nombres = new ArrayList<>();
        nombres.add("Luis Perez");
        nombres.add("Paula Negrette");
        nombres.add("Jaime Diaz");
        
        for (String nombre : nombres) {
            System.out.println(nombre);
        }
        
        Iterator<String> iterator = nombres.iterator();
        while(iterator.hasNext())
            System.out.println(iterator.next());
      
        
        // Objetos creados con una clase
        Persona persona1 = new Persona("Jaime", "Diaz", "70035135");
        Persona persona2 = new Persona("Paula", "Negrette", "125478963");
        Persona persona3 = new Persona("Pablo", "Ruiz", "1023654789");
        List<Persona> personas = new ArrayList<>();
        personas.add(persona1);
        personas.add(persona2);
        personas.add(persona3);
        
        for (Persona persona : personas) {
            System.out.println(persona);
        }
    } 
}

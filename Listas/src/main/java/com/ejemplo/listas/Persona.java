/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ejemplo.listas;

/**
 *
 * @author YURA
 */
public class Persona {
    private String nombre;
    private String apellido;
    private String noDocumento;

    public Persona(String nombre, String apellido, String noDocumento) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.noDocumento = noDocumento;
    }

    public Persona() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNoDocumento() {
        return noDocumento;
    }

    public void setNoDocumento(String noDocumento) {
        this.noDocumento = noDocumento;
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", apellido=" + apellido + ", noDocumento=" + noDocumento + '}';
    }
    
}

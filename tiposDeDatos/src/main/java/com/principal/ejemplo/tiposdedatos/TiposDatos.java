/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.principal.ejemplo.tiposdedatos;

/**
 *
 * @author aiya
 */
public class TiposDatos {
    
    public static void main(String[] args) {
        /* Tipos de datos primitivos */
        /* Enteros */
        /* Tipo de datos y el nombre */
        // Byte
        byte numeroEnteroByte; 
        numeroEnteroByte = -128;
        System.out.println("Tipo de dato Byte minimo -> "+numeroEnteroByte);
        numeroEnteroByte = 127;
        System.out.println("Tipo de dato Byte máximo-> "+numeroEnteroByte);
        // 2 ^ 8 = 256 -> byte tiene 8 bits
        /*Tipos de datos objetos*/
        
        // Short
        short numeroEnteroShort;
        numeroEnteroShort = -32768;
        System.out.println("Tipo de dato Short minimo -> "+numeroEnteroShort);
        numeroEnteroShort = 32767;
        System.out.println("Tipo de dato Short máximo-> "+numeroEnteroShort);
        // 2 ^ 16 = 655365
        
        // Integer
        int numeroEnteroInt;
        numeroEnteroInt = - 2147483648;
        System.out.println("Tipo de dato Int minimo -> "+numeroEnteroInt);
        numeroEnteroInt = 2147483647;
        System.out.println("Tipo de dato Int máximo -> "+numeroEnteroInt);
        // 2 ^ 32 = 4.294.967.296
        
        // long
        long numeroEnteroLong;
        numeroEnteroLong = - 9223372036854775808L;
        System.out.println("Tipo de dato long minimo -> "+numeroEnteroLong);
        numeroEnteroLong = 9223372036854775807L;
        System.out.println("Tipo de dato long máximo -> "+numeroEnteroLong);
        // 2 ^ 64 = 18.446.744.073.709.551.616
        
        
    }
    
    
}
